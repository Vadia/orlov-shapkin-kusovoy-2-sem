<?php
require 'php/db/db.php';

$data = $_POST;
$mess = 0;

if ( isset($data['btn']) )
	{
		$errors = array();
		if ( trim($data['login']) == '' )
		{
			$errors[] = 'Введите логин';
		}

		if ( trim($data['email']) == '' )
		{
			$errors[] = 'Введите Email';
		}

		if ( $data['pass'] == '' )
		{
			$errors[] = 'Введите пароль';
		}
		
		if ( $data['name'] == '' )
		{
			$errors[] = 'Введите пароль';
		}

		if ( $data['pass2'] != $data['pass'] )
		{
			$errors[] = 'Повторный пароль введен не верно!';
		}

		//проверка на существование одинакового логина
		if ( R::count('users', "login = ?", array($data['login'])) > 0)
		{
			$errors[] = 'Пользователь с таким логином уже существует!';
		}
    
    //проверка на существование одинакового email
		if ( R::count('users', "email = ?", array($data['email'])) > 0)
		{
			$errors[] = 'Пользователь с таким Email уже существует!';
		}
		
		if ( empty($errors) )
		{
			//ошибок нет, теперь регистрируем
			$user = R::dispense('users');
			$user->login = $data['login'];
			$user->email = $data['email'];
			$user->phone = $data['phone'];
			$user->pass = password_hash($data['pass'], PASSWORD_DEFAULT); //пароль нельзя хранить в открытом виде, мы его шифруем
			$user->name = $data['name'];
			$user->pet = $data['pet'];
			$user->namepet = $data['namepet'];
			$user->grp = "user";
			R::store($user);
			
			header('Location: login.php ');
		}else
		{
			//вывод ошибок
			$mess = 1;
		}

		
	}
 ?>
<html>
<head>
<title>Регистрация</title>
<link rel="stylesheet" href="assets/css/app.css">
<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
</head>
<body class="light">
<div id="primary" class="p-t-b-100 height-full ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto">
                    <div class="text-center">
                        <img src="assets/img/u1.png" alt="">
                        <h3 class="mt-2">Добро пожаловать</h3>
						
						<?php 	if ( $mess == 1) : ?>
					<?php echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div>';  ?>
						<?php endif;?>
						
						<hr color="black" width="300">
                       <a href="/login.php" ><p class="p-t-b-20">У вас есть учетная запись? Войти</p></a>
                    </div>
                    <form method="POST">
                        <div class="form-group has-icon">
                            <input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control form-control-lg"
                                   placeholder="Логин">
                        </div>
                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                            <input type="text" name="email" value="<?php echo @$data['email']; ?>" class="form-control form-control-lg"
                                   placeholder="Email">
                        </div>
						<div class="form-group has-icon">
                            <input type="text" name="phone" value="<?php echo @$data['phone']; ?>" class="form-control form-control-lg"
                                   placeholder="Телефон">
                        </div>
						<div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input type="password" name="pass" value="<?php echo @$data['pass']; ?>" class="form-control form-control-lg"
                                   placeholder="Пароль">
                        </div>
						<div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input type="password" name="pass2" value="<?php echo @$data['pass2']; ?>" class="form-control form-control-lg"
                                   placeholder="Повтор пароля">
                        </div>
						<div class="form-group has-icon">
                            <input type="text" name="name" value="<?php echo @$data['name']; ?>" class="form-control form-control-lg"
                                   placeholder="Ваше имя">
                        </div>
						<div class="form-group has-icon">
                            <input type="text" name="pet" value="<?php echo @$data['pet']; ?>" class="form-control form-control-lg"
                                   placeholder="Кто ваш питомец?">
                        </div>
						<div class="form-group has-icon">
                            <input type="text" name="namepet" value="<?php echo @$data['namepet']; ?>" class="form-control form-control-lg"
                                   placeholder="Имя вашего питомца">
                        </div>
                        <input type="submit" name="btn" class="btn btn-success btn-lg btn-block">
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>	
</html>