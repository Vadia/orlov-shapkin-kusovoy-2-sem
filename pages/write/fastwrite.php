<?php
$date = $_GET['date'];
$time = $_GET['time'];
$otherdate = $_GET['otherdate'];
 ?>

<body>

<header>       
		<div id="noscroll" class="header-area ">
            <div class="header-top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#">+7 (999)999-99-99</a></li>
                                    <li><a href="#">Пн-Сб 8:00 - 22:00</a></li>
									<?php if(isset($_SESSION['logged_user'])): ?>
									<li><a href="/lk.php">ЛК</a></li>
									<?php else: ?>
									<li><a href="/login.php">Вход</a></li>
									<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 ">
                            <div class="social_media_links">
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-pinterest-p"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-3">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="assets/img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a  href="index.php">Домой</a></li>
										<li><a href="?blog=1">Блог</a></li>
                                        <li><a href="?contact=1">Контакты</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
	
	<?php if(isset($otherdate)): ?>
	<?php include_once 'otherdate.php'; ?>
	<?php else: ?>
	
	<?php if(isset($time)):?>
	<?php include_once 'writeonday.php'; ?>
	<?php else: ?>
	<?php include_once 'startwrite.php'; ?>
	
	<?php endif; ?><?php endif; ?>
	
	
	<div class="copy-right_text">
            <div class="container">
                <div class="bordered_1px"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <p>
   &copy; 2021 Все права защищены | by <a href="" target="_blank">Orlov|Shapkin</a>
  </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>


</body>