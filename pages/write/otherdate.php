<?php
 ?>
 
    
 
    <section class="u-clearfix u-section-3" id="carousel_f568">
      <div class="u-clearfix u-sheet u-sheet-1">
        <h1 class="u-align-center u-custom-font u-font-oswald u-text u-text-default u-text-1">Выберете интересующую вас дату:</h1>
      </div>
    </section>
    <section class="u-align-center u-clearfix u-section-4" id="sec-e581">
      <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
        <div class="u-form u-form-1">
          <form action="" class="u-clearfix u-form-horizontal u-form-spacing-15 u-inner-form" style="padding: 15px" source="custom">
            <div class="u-form-date u-form-group u-form-group-1">
              <label for="date-0ae9" class="u-form-control-hidden u-label">Дата</label>
              <input type="date" placeholder="YYYY-MM-DD" id="input-date-1" name="date" class="u-border-1 u-border-grey-30 u-input u-input-rectangle" required="">
            </div>
            <div class="u-form-group u-form-submit u-form-group-2">
              <a id="btn-select-date-1" class="u-btn u-btn-submit u-button-style">Выбрать<br></a>
            </div>
			<br><br>
		<div id="echoerrors"> </div>
            
          </form>
        </div>
      </div>
    </section>
	
<div id="showblock-1" style="visibility: visible;">

    <section class="u-clearfix u-section-5" id="carousel_82fc">
      <div class="u-clearfix u-sheet u-sheet-1">
        <h1 class="u-align-center u-custom-font u-font-oswald u-text u-text-default u-text-1">Запись на <a id="zapisna"></a>:</h1>
      </div>
    </section>
	
    <section class="u-clearfix u-section-2" id="sec-79c5">
      <div class="u-clearfix u-sheet u-valign-top u-sheet-1">
        <div class="u-expanded-width u-layout-horizontal u-list u-list-1">
          <div class="u-repeater u-repeater-1">
	


			<div id="outblock-10" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">10:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center><h3 id="namedoctor-10" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-10" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

            <div id="outblock-11" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">11:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-11" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-11" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-12" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">12:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-12" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-12" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-13" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">13:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-13" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-13" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-14" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">14:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
                <center><h3 id="namedoctor-14" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-14" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-15" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">15:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-15" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-15" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-16" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">16:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-16" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-16" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-17" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">17:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-17" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-17" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-18" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">18:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-18" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-18" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-19" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">19:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-19" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-19" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-20" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">20:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
              <center>  <h3 id="namedoctor-20" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-20" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

             <div id="outblock-21" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">21:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
                <center><h3 id="namedoctor-21" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-21" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>

           <div id="outblock-22" style="display: block;" class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1">22:00</h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
               <center> <h3 id="namedoctor-22" class="u-text u-text-default u-text-3"></h3></center>
                <a id="hrefblock-22" href="" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>
            
          </div>
          <a class="u-absolute-vcenter u-gallery-nav u-gallery-nav-prev u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-gallery-nav-1" href="#" role="button">
            <span aria-hidden="true">
              <svg viewBox="0 0 451.847 451.847"><path d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z"></path></svg>
            </span>
            <span class="sr-only">
              <svg viewBox="0 0 451.847 451.847"><path d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z"></path></svg>
            </span>
          </a>
          <a class="u-absolute-vcenter u-gallery-nav u-gallery-nav-next u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-gallery-nav-2" href="#" role="button">
            <span aria-hidden="true">
              <svg viewBox="0 0 451.846 451.847"><path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"></path></svg>
            </span>
            <span class="sr-only">
              <svg viewBox="0 0 451.846 451.847"><path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"></path></svg>
            </span>
          </a>
        </div>
       
      </div>
    </section>
  </div>  
	
    	

	 <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="pages/write/write.js"></script>