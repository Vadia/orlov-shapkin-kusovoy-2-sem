<?php ?>

<section class="u-clearfix u-section-7" id="carousel_668a">
      <div class="u-clearfix u-sheet u-sheet-1">
	  <h1 class="u-align-center u-custom-font u-font-oswald u-text u-text-1">Дата и время записи:</h1>
	  <h1 id="Kdatetime" class="u-align-center u-custom-font u-font-oswald u-text u-text-1"><?php echo $date; ?> / <?php echo $time; ?></h1><hr>
        <h1 class="u-align-center u-custom-font u-font-oswald u-text u-text-1">Оставьте свои контакты:</h1>
      </div>
    </section>
    <section class="u-align-center u-clearfix u-section-8" id="sec-f858">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-center u-form u-form-1">
          <form action="" class="u-clearfix u-form-horizontal u-form-spacing-15 u-inner-form" style="padding: 15px" source="custom">
            <div class="u-form-group u-form-name u-form-group-1">
              <label for="name-558c" class="u-form-control-hidden u-label">Name</label>
              <input id="Kname" type="text" placeholder="Имя" id="name-558c" name="name" class="u-border-1 u-border-grey-30 u-input u-input-rectangle" required="">
            </div>
            <div class="u-form-group u-form-phone u-form-group-2">
              <label for="email-558c" class="u-form-control-hidden u-label">Phone</label>
              <input id="Kphone" type="tel" placeholder="Номер телефона" id="email-558c" name="phone" class="u-border-1 u-border-grey-30 u-input u-input-rectangle" required="required">
            </div>
            <div class="u-form-group u-form-submit u-form-group-3">
              <a id="gotowriteklient" href="" class="u-btn u-btn-submit u-button-style">Записаться<br></a>
            </div>
            <div id="errorsout"> </div>
            <div class="u-form-send-message u-form-send-success">Вы успешно записались на прием к доктору. Спасибо!</div>
            <div class="u-form-send-error u-form-send-message">Вы вводите некорректные данные!</div>
            
          </form>
        </div>
      </div>
    </section>

    <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="pages/write/write.js"></script>