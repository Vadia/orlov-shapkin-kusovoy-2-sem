<?php 
//Статусы: 0 - новая заявка. 1 -.  3 -.


//получаем свободные даты
$nowdate = date("Y-m-d");
$ttimes = array(10=>'10:00',11=>'11:00',12=>'12:00',13=>'13:00',14=>'14:00',15=>'15:00',16=>'16:00',17=>'17:00',18=>'18:00',19=>'19:00',20=>'20:00',21=>'21:00',22=>'22:00');
$findzapis = R::getAll("SELECT * FROM `zapis` WHERE date = '$nowdate';");

foreach ($findzapis as $message){
	if(strlen($message['time']) == 4){
	$inx = substr($message['time'],0,1);
	}else{
	$inx = substr($message['time'],0,2);
	}
	unset($ttimes[$inx]);
}


 ?>

 <section class="u-clearfix u-section-1" id="sec-3d11">
      <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
        <h1 class="u-align-center u-custom-font u-font-oswald u-text u-text-default u-text-1">Запись на сегодня(<?php echo $nowdate; ?>):</h1>
      </div>
 </section>
	
    <section class="u-clearfix u-section-2" id="sec-79c5">
      <div class="u-clearfix u-sheet u-valign-top u-sheet-1">
        <div class="u-expanded-width u-layout-horizontal u-list u-list-1">
          <div class="u-repeater u-repeater-1">
		  
		  <?php foreach ($ttimes as $time):
		  
		  $InfoAboutTime = R::getAll("SELECT * FROM `DoctorRasp` WHERE date = '$nowdate' AND time = '$time';"); ?>

		<?php  if($InfoAboutTime != null) :?>


<?php 
foreach ($InfoAboutTime as $infat){
$docname = $infat['doctor'];
}
?>


  
            <div class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1"><?php echo $time; ?></h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
         
               <center><h3 class="u-text u-text-default u-text-3"><?php echo $docname; ?></h3></center>
         
                <a href="index.php?gowrite=1&date=<?php echo $nowdate; ?>&time=<?php echo $time; ?>" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>
  
			<?php else: ?>
			
			<div class="u-align-center u-container-style u-list-item u-repeater-item u-list-item-1" data-animation-name="pulse" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <h2 class="u-text u-text-default u-text-palette-2-light-1 u-text-1"><?php echo $time; ?></h2>
                <h3 class="u-text u-text-default u-text-palette-1-light-1 u-text-2">Доктор</h3>
                <center><h3 class="u-text u-text-default u-text-3">На уточнении</h3></center>
                <a href="index.php?gowrite=1&date=<?php echo $nowdate; ?>&time=<?php echo $time; ?>" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-1">Записаться</a>
              </div>
            </div>
			<?php endif; ?>
			
			<?php endforeach; ?>
            
          </div>
          <a class="u-absolute-vcenter u-gallery-nav u-gallery-nav-prev u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-gallery-nav-1" href="#" role="button">
            <span aria-hidden="true">
              <svg viewBox="0 0 451.847 451.847"><path d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z"></path></svg>
            </span>
            <span class="sr-only">
              <svg viewBox="0 0 451.847 451.847"><path d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z"></path></svg>
            </span>
          </a>
          <a class="u-absolute-vcenter u-gallery-nav u-gallery-nav-next u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-gallery-nav-2" href="#" role="button">
            <span aria-hidden="true">
              <svg viewBox="0 0 451.846 451.847"><path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"></path></svg>
            </span>
            <span class="sr-only">
              <svg viewBox="0 0 451.846 451.847"><path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"></path></svg>
            </span>
          </a>
        </div>
       <a href="index.php?gowrite=1&otherdate=1" class="u-align-center u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-4">Запись на другие даты</a>
      </div>
    </section>
	
	<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
	<script src="pages/write/write.js"></script>