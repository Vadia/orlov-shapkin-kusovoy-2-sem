<?php
$data = $_POST;

if ( isset($data['do_mess']) ){
	
	$mess = R::dispense('messagesfromcontact');
	$mess->name = $data['name'];
	$mess->email = $data['email'];
	$mess->text = $data['message'];
	R::store($mess);
	
}
 ?>

<div class="bradcam_area breadcam_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcam_text text-center">
                        <h3>Контакты</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end -->

    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="d-none d-sm-block mb-5 pb-4">
                    <div id="map" style="height: 480px; position: relative; overflow: hidden;"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4618.891265536574!2d37.70853403895662!3d55.781077516368555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54ad610abc8a5%3A0xc42703145fe53a8d!2z0JzQvtGB0LrQvtCy0YHQutC40Lkg0L_QvtC70LjRgtC10YXQvdC40YfQtdGB0LrQuNC5INGD0L3QuNCy0LXRgNGB0LjRgtC10YI!5e0!3m2!1sru!2sru!4v1625765887770!5m2!1sru!2sru" width="1350" height="800" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>   
    
                </div>
    
    
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Ждем ваших обращений!</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" method="POST" id="contactForm" novalidate="novalidate">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="message" value="<?php echo @$data['message']; ?>" cols="30" rows="9" placeholder=" Сообщение"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" value="<?php echo @$data['name']; ?>" type="text" placeholder="Введите ваше имя">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="email" value="<?php echo @$data['email']; ?>" type="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" name="do_mess" class="button button-contactForm boxed-btn">Отправить</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Московский политех</h3>
                                <p>Большая семеновская 38</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+7(999)999-99-99</h3>
                                <p>Звоните всегда.</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>support@polytech.ru</h3>
                                <p>Напишите нам на почту.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>