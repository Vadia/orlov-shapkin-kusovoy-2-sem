<?php
$category = $_GET['category'];

$data = $_POST;

 ?>
<div class="bradcam_area breadcam_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcam_text text-center">
                        <h3>Блог</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="blog_area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
<?php					
if ( isset($data['serchtextbd']) ):

	$zapros = $data['qest'];
	$list11 = R::getAll("SELECT * FROM `blogs` WHERE name LIKE '%$zapros%'");
	
	foreach ($list11 as $row):			
?>
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="<?php echo $row['srcimg']; ?>" alt="">
                                <a href="#" class="blog_item_date">
                                    <p><?php echo $row['date']; ?></p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="">
                                    <h2><?php echo $row['name']; ?></h2>
                                </a>
                                <p><?php echo $row['text']; ?></p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-certificate"></i> <?php echo $row['category']; ?></a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> <?php echo $row['vives']; ?></a></li>
                                </ul>
                            </div>
                        </article>
<?php endforeach; ?> 
		<?php else: ?>		

<?php if(isset($category)): ?>

<?php
$list = R::getAll("SELECT * FROM `blogs` WHERE `category` = '$category' ORDER BY vives DESC LIMIT 3;");
 foreach ($list as $row): ?>
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="<?php echo $row['srcimg']; ?>" alt="">
                                <a href="#" class="blog_item_date">
                                    <p><?php echo $row['date']; ?></p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="">
                                    <h2><?php echo $row['name']; ?></h2>
                                </a>
                                <p><?php echo $row['text']; ?></p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-certificate"></i> <?php echo $row['category']; ?></a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> <?php echo $row['vives']; ?></a></li>
                                </ul>
                            </div>
                        </article>
					<?php endforeach; ?> 


<?php else: ?>					
<?php
$list = R::getAll("SELECT * FROM `blogs` ORDER BY vives DESC LIMIT 3;");
 foreach ($list as $row): ?>
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="<?php echo $row['srcimg']; ?>" alt="">
                                <a href="#" class="blog_item_date">
                                    <p><?php echo $row['date']; ?></p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="">
                                    <h2><?php echo $row['name']; ?></h2>
                                </a>
                                <p><?php echo $row['text']; ?></p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-certificate"></i> <?php echo $row['category']; ?></a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> <?php echo $row['vives']; ?></a></li>
                                </ul>
                            </div>
                        </article>
					<?php endforeach; ?> 	
<?php endif; ?>  
<?php endif; ?>                     
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <form method="POST" action="#">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input name="qest" value="<?php echo @$data['qest']; ?>" type="text" class="form-control" placeholder='Поиск..'>
                                    </div>
                                </div>
                                <a href="?search=1"><button name="serchtextbd" class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
                                    type="submit">Найти</button></a>
                            </form>
                        </aside>


                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Категории</h4>
                            <ul class="list cat-list">
                                <li>
                                    <a href="index.php?blog=1&category=Собаки" class="d-flex">
                                        <p>Собаки</p>
                                        <p>(37)</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?blog=1&category=Кошки" class="d-flex">
                                        <p>Кошки</p>
                                        <p>(10)</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php?blog=1&category=Морские свинки" class="d-flex">
                                        <p>Морские свинки</p>
                                        <p>(3)</p>
                                    </a>
                                </li>
                            </ul>
                        </aside>


                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Популярное</h3>
                            <div class="media post_item">
                                <img src="pages/blogimg/img1.jpg" width="75" height="75" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Коты</h3>
                                    </a>
                                    <p>7 часов назад</p>
                                </div>
                            </div>
                            <div class="media post_item">
                                <img src="pages/blogimg/img2.jpg" width="75" height="75" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Морские свинки</h3>
                                    </a>
                                    <p>2 часа назад</p>
                                </div>
                            </div>
                            <div class="media post_item">
                                <img src="pages/blogimg/img3.jpg" width="75" height="75" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Собаки</h3>
                                    </a>
                                    <p>3 часа назад</p>
                                </div>
                            </div>
                        </aside>
                        

                        
                    </div>
                </div>
            </div>
        </div>
    </section>