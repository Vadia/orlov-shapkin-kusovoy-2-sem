<?php
$login = $_SESSION['logged_user']->login;
$finddoctors = R::getAll("SELECT * FROM `users` WHERE grp = 'doctor' AND login = '$login';");
 ?>


<section class="u-align-center u-clearfix u-white u-section-1" id="sec-30b5">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-expanded-width u-tab-links-align-justify u-tabs u-tabs-1">
          <ul class="u-tab-list u-unstyled" role="tablist">
            <li class="u-tab-item" role="presentation">
              <a class="active u-active-white u-border-2 u-border-active-palette-2-base u-border-grey-15 u-border-hover-grey-15 u-border-no-bottom u-border-no-left u-border-no-right u-button-style u-grey-15 u-hover-grey-15 u-tab-link u-tab-link-1" id="link-tab-0da5" href="#tab-0da5" role="tab" aria-controls="tab-0da5" aria-selected="true">График работы</a>
            </li>
            
          </ul>
          <div class="u-tab-content">
            <div class="u-container-style u-tab-active u-tab-pane u-white u-tab-pane-1" id="tab-0da5" role="tabpanel" aria-labelledby="link-tab-0da5">
              <div class="u-container-layout u-container-layout-1">
                <h3 class="u-text u-text-default u-text-1">Ваше расписание</h3>
                <div class="u-form u-form-1">
                  <form action="" class="u-clearfix u-form-spacing-10 u-form-vertical u-inner-form" style="padding: 10px" source="custom" name="form">
                    <input type="hidden" id="siteId" name="siteId" value="126023929">
                    <input type="hidden" id="pageId" name="pageId" value="71664605">
                    <div class="u-form-group u-form-select u-form-group-1">
                      <label for="select-aefc" class="u-form-control-hidden u-label">Выбрать</label>
                      <div class="u-form-select-wrapper">
                        <select id="select-1" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
						<?php foreach ($finddoctors as $dfocs): ?>
                          <option value="<?php echo $dfocs['id'] ?>"><?php echo $dfocs['name'] ?></option>
                        <?php endforeach; ?>
                        </select>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
                      </div>
                    </div>
                    <div class="u-align-center u-form-group u-form-submit">
                      <a id="finddocrasp" href="" class="u-btn u-btn-submit u-button-style">Отправить</a>
                    </div>  
                  </form>
                </div>
                <div class="u-expanded-width u-table u-table-responsive u-table-1">
                  <table class="u-table-entity">
                    <colgroup>
                      <col width="33.3%">
                      <col width="33.3%">
                      <col width="33.400000000000006%">
                    </colgroup>
                    <thead class="u-align-center u-palette-1-base u-table-header u-table-header-1">
                      <tr style="height: 29px;">
                        <th class="u-table-cell">Сотрудник</th>
                        <th class="u-table-cell">Дата</th>
                        <th class="u-table-cell">Рабочие часы</th>
                      </tr>
                    </thead>
                    <tbody id="addnewrasp" class="u-align-center u-table-alt-palette-1-light-3 u-table-body">
					
                      
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>