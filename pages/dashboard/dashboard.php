<?php
$priemnow = R::count( 'zapis', 'date = ?', [Date('Y-m-d')] );

$priem30days = R::count( 'zapis', 'date > ?', [Date('Y-m-d') - 1] );

/* доход
$dohod = 0;
$date1= Date('Y-m-d') - 1;
$finddohod = R::getAll("SELECT * FROM `zapis` WHERE date > '$date1'");
foreach($finddohod as $fdhd){

}
*/
 ?>
<html lang="ru">

<?php if(isset ($_SESSION['logged_user'])): ?>
<?php if($_SESSION['logged_user']->grp == "admin"):?>
<body>
  <div class="container-scroller">
  
    
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <!-- partial -->
	  <center>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="row">
                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                  <h3 class="font-weight-bold">Здравствуйте <?php echo $_SESSION['logged_user']->name; ?></h3>
                  <!--<h6 class="font-weight-normal mb-0">All systems are running smoothly! You have <span class="text-primary">3 unread alerts!</span></h6> -->
                </div>
                <div class="col-12 col-xl-4">
                 <div class="justify-content-end d-flex">
                  <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                    <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                     <i class="mdi mdi-calendar"></i> Сегодня (<?php echo Date("d.m.Y"); ?>)
                    </button>
                  </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card tale-bg">
                <div class="card-people mt-auto">
                  <img src="pages/dashboard/images/dashboard/people.svg" alt="people">
                  <div class="weather-info">
                    <div class="d-flex">
                      <div>
                       <h2 class="mb-0 font-weight-normal"><?php echo date("H:i"); ?>&nbsp; &nbsp; &nbsp; &nbsp;</h2>
                      </div>
                      <div class="ml-2">
                        <h4 class="location font-weight-normal">Москва</h4>
                        <h6 class="font-weight-normal">Россия</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 grid-margin transparent">
              <div class="row">
                <div class="col-md-6 mb-4 stretch-card transparent">
                  <div class="card card-tale">
                    <div class="card-body">
                      <p class="mb-4">Приемов сегодня</p>
                      <p class="fs-30 mb-2"><?php echo $priemnow; ?></p>
                      
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mb-4 stretch-card transparent">
                  <div class="card card-dark-blue">
                    <div class="card-body">
                      <p class="mb-4">приемов за месяц</p>
                      <p class="fs-30 mb-2"><?php echo $priem30days; ?></p>
                      <p>(30 дней)</p>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Статистика приемов за месяц</p>
                  <p class="font-weight-500">На графике показано сравнение количества приемов за за этот и предыдуший месяцы. по дням.</p>
                   <div class="d-flex flex-wrap mb-5">
                    <div class="mr-5 mt-3">
                      <p class="text-muted">Количество приемов</p>
                      <h3 class="text-primary fs-30 font-weight-medium"><?php echo $priem30days; ?></h3>
                    </div>
                    <div class="mr-5 mt-3">
                      <p class="text-muted">Доход</p>
                      <h3 class="text-primary fs-30 font-weight-medium">9000р</h3>
                    </div>
                    <div class="mr-5 mt-3">
                      <p class="text-muted">Средний доход на прием</p>
                      <h3 class="text-primary fs-30 font-weight-medium">900р</h3>
                    </div>
                  </div>
                  <canvas id="order-chart"></canvas> 
                </div>
              </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                 <div class="d-flex justify-content-between">
                  <p class="card-title">План роста средней цены приема</p>
                  
                 </div>
                  <p class="font-weight-500">План / факт средней цены приема.</p>
                  <div id="sales-legend" class="chartjs-legend mt-4 mb-2"></div>
                  <canvas id="sales-chart"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card position-relative">
                <div class="card-body">
                  <div id="detailedReports" class="carousel slide detailed-report-carousel position-static pt-2" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <div class="row">
                          <div class="col-md-12 col-xl-3 d-flex flex-column justify-content-start">
                            <div class="ml-xl-4 mt-3">
                            <p class="card-title">Прогнозируемый доход</p>
                              <h1 class="text-primary">125500р</h1>
                              <h3 class="font-weight-500 mb-xl-4 text-primary">На <?php echo Date("Y-m") ?></h3>
                              <p class="mb-2 mb-xl-0">Модуль в разработке</p>
                            </div>  
                            </div>
                          <div class="col-md-12 col-xl-9">
                            <div class="row">
                              <div class="col-md-6 border-right">
                                <div class="table-responsive mb-3 mb-md-0 mt-3">
                                  <table class="table table-borderless report-table">
                                    <tr>
                                      <td class="text-muted">Проведение операций</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-primary" role="progressbar" style="width: 45%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">52500р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Продажа лекарств</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-warning" role="progressbar" style="width: 8%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">10000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Приемы</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-danger" role="progressbar" style="width: 39%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">40000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Продажа кормов</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-info" role="progressbar" style="width: 6%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">8000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Передержка</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-primary" role="progressbar" style="width: 8%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">10000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Буффет</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-danger" role="progressbar" style="width: 4%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">5000р</h5></td>
                                    </tr>
                                  </table>
                                </div>
                              </div>
                              <div class="col-md-6 mt-3">
                                <canvas id="north-america-chart"></canvas>
                                <div id="north-america-legend"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <div class="row">
                          <div class="col-md-12 col-xl-3 d-flex flex-column justify-content-start">
                            <div class="ml-xl-4 mt-3">
                            <p class="card-title">Прогнозируемый доход</p>
                              <h1 class="text-primary">132600р</h1>
                              <h3 class="font-weight-500 mb-xl-4 text-primary">На <?php echo Date("Y-m", strtotime("+1 months")); ?></h3>
                              <p class="mb-2 mb-xl-0">Модуль в разработке</p>
                            </div>  
                            </div>
                          <div class="col-md-12 col-xl-9">
                            <div class="row">
                              <div class="col-md-6 border-right">
                                <div class="table-responsive mb-3 mb-md-0 mt-3">
                                  <table class="table table-borderless report-table">
                                    <tr>
                                      <td class="text-muted">Проведение операций</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-primary" role="progressbar" style="width: 45%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">52500р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Продажа лекарств</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-warning" role="progressbar" style="width: 8%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">10000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Приемы</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-danger" role="progressbar" style="width: 39%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">40000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Продажа кормов</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-info" role="progressbar" style="width: 6%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">8000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Передержка</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-primary" role="progressbar" style="width: 16%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">20000р</h5></td>
                                    </tr>
                                    <tr>
                                      <td class="text-muted">Буффет</td>
                                      <td class="w-100 px-0">
                                        <div class="progress progress-md mx-4">
                                          <div class="progress-bar bg-danger" role="progressbar" style="width: 4%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </td>
                                      <td><h5 class="font-weight-bold mb-0">5000р</h5></td>
                                    </tr>
                                  </table>
                                </div>
                              </div>
                              <div class="col-md-6 mt-3">
                                <canvas id="south-america-chart"></canvas>
                                <div id="south-america-legend"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#detailedReports" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Назад</span>
                    </a>
                    <a class="carousel-control-next" href="#detailedReports" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Далее</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title mb-0">Исполнение заявок</p>
                  <div class="table-responsive">
                    <table class="table table-striped table-borderless">
                      <thead>
                        <tr>
                          <th>Доктор</th>
                          <th>Цена</th>
                          <th>Дата</th>
                          <th>Статус</th>
                        </tr>  
                      </thead>
                      <tbody>
					  <?php $findzayavki = R::getAll("SELECT * FROM `zapis` ORDER BY id DESC LIMIT 7;"); ?>
					    <?php foreach($findzayavki as $fz): ?>
                        <tr>
                          <td><?php echo $fz['doctor']; ?></td>
                          <td class="font-weight-bold"><?php echo $fz['price']; ?></td>
                          <td><?php echo $fz['date']; ?></td>
						  <?php if($fz['status'] == 0){
							  echo'<td class="font-weight-medium"><div class="badge badge-warning">В процессе</div></td>';
						  }elseif($fz['status'] == 1){
							  echo'<td class="font-weight-medium"><div class="badge badge-danger">Отклонено</div></td>';
						  }elseif($fz['status'] == 3){
							  echo'<td class="font-weight-medium"><div class="badge badge-success">Завершено</div></td>';
						  }
						  ?>
                          
						  
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-5 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Список дел</h4>
									<div class="list-wrapper pt-2">
										<ul class="d-flex flex-column-reverse todo-list todo-list-custom">
										
									<?php $findtodo = R::getAll("SELECT * FROM `todolist` ORDER BY id DESC;"); ?>
					                    <?php foreach($findtodo as $ftd):
										$i = '';
										$ss = '';
                                        if($ftd['checkbox'] == 1){
									    
										}elseif($ftd['checkbox'] == 2){
											$i = 'checked';
											$ss = 'completed';
										}

										?>
											<li class="<?php echo $ss; ?>">
												<div class="form-check form-check-flat">
													<label class="form-check-label">
														<input id="<?php echo $ftd['id']; ?>" class="checkbox" type="checkbox" <?php echo $i; ?>>
														<?php echo $ftd['text']; ?>
													</label>
												</div>
												<i id="<?php echo $ftd['id']; ?>" class="remove ti-close"></i>
											</li>
										<?php endforeach; ?>
	
										</ul>
                  </div>
                  <div class="add-items d-flex mb-0 mt-2">
										<input id="texttodolist" type="text" class="form-control todo-list-input"  placeholder="Добавить новое задание">
										<button id="addtotodolist" class="add btn btn-icon text-primary todo-list-add-btn bg-transparent"><i class="icon-circle-plus"></i></button>
									</div>
								</div>
							</div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card">
                <div class="card-body">
                  <p class="card-title mb-0">Сессии сотрудников</p>
                  <div class="table-responsive">
                    <table class="table table-borderless">
                      <thead>
                        <tr>
                          <th class="pl-0  pb-2 border-bottom">Логин</th>
                          <th class="border-bottom pb-2">Группа</th>
                          <th class="border-bottom pb-2">Дата/Время</th>
                        </tr>
                      </thead>
                      <tbody>
					  
					  <?php $findsess= R::getAll("SELECT * FROM `authsess` ORDER BY id DESC LIMIT 12;"); ?>
					  
					    <?php foreach($findsess as $session): ?>
					  
                        <tr>
                          <td class="pl-0"><?php echo $session['login']; ?></td>
                          <td><?php echo $session['grp']; ?></td>
                          <td class="text-muted"><?php echo $session['datetime']; ?></td>
                        </tr>
						<?php endforeach; ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title">Исполнение плана приемов</p>
                      <div class="charts-data">
					  
					  
                        <div class="mt-3">
                          <p class="mb-0">План на Январь</p>
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="progress progress-md flex-grow-1 mr-4">
                              <div class="progress-bar bg-inf0" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="mb-0">150</p>
                          </div>
                        </div>
						
						<div class="mt-3">
                          <p class="mb-0">Факт на Январь</p>
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="progress progress-md flex-grow-1 mr-4">
                              <div class="progress-bar bg-inf0" role="progressbar" style="width: 75%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="mb-0">115</p>
                          </div>
                        </div>
						
						<hr>
						
						<div class="mt-3">
                          <p class="mb-0">План на Декабрь</p>
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="progress progress-md flex-grow-1 mr-4">
                              <div class="progress-bar bg-inf0" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="mb-0">140</p>
                          </div>
                        </div>
						
						<div class="mt-3">
                          <p class="mb-0">Факт на Декабрь</p>
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="progress progress-md flex-grow-1 mr-4">
                              <div class="progress-bar bg-inf0" role="progressbar" style="width: 80%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p class="mb-0">112</p>
                          </div>
                        </div>
                        
                      </div>  
                    </div>
                  </div>
                </div>
                <div class="col-md-12 stretch-card grid-margin grid-margin-md-0">
                  <div class="card data-icon-card-primary">
                    <div class="card-body">
                      <p class="card-title text-white">Количество обращений пользователей</p>                      
                      <div class="row">
                        <div class="col-8 text-white">
						<?php $countmess = R::count( 'messagesfromcontact' ); ?>
                          <h3><?php echo $countmess; ?></h3>
                          <p class="text-white font-weight-500 mb-0">Общее кол-во обращений пользователей через контактную форму за всю историю.</p>
                        </div>
                        <div class="col-4 background-icon">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Последние обращения</p>
                  <ul class="icon-data-list">
                    <?php
            $findmessages = R::getAll("SELECT * FROM `messagesfromcontact` ORDER BY id DESC LIMIT 5;");
			
					?>
					<?php foreach($findmessages as $fm):
                         $mintext = mb_strimwidth($fm['text'], 0, 40, "...");
					?>
					<li>
                      <div class="d-flex">
                        <img src="pages/dashboard/images/faces/face1.jpg" alt="user">
                        <div>
                          <p class="text-info mb-1"><?php echo $fm['name'];?></p>
                          <p class="mb-0"><?php echo $mintext;?></p>
                          <small><?php echo $fm['email'];?></small>
                        </div>
                      </div>
                    </li>
					<?php endforeach; ?>
                    
					
                  </ul>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div></center>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="pages/dashboard/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="pages/dashboard/vendors/chart.js/Chart.min.js"></script>
  <script src="pages/dashboard/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="pages/dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="pages/dashboard/js/dataTables.select.min.js"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="pages/dashboard/js/off-canvas.js"></script>
  <script src="pages/dashboard/js/hoverable-collapse.js"></script>
  <script src="pages/dashboard/js/template.js"></script>
  <script src="pages/dashboard/js/settings.js"></script>
  <script src="pages/dashboard/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="pages/dashboard/js/dashboard.js"></script>
  <script src="pages/dashboard/js/Chart.roundedBarCharts.js"></script>
  <!-- End custom js for this page-->
</body>
<?php endif; ?>
<?php endif; ?>
</html>

