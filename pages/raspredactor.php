<?php
$finddoctors = R::getAll("SELECT * FROM `users` WHERE grp = 'doctor'");
 ?>


<section class="u-align-center u-clearfix u-white u-section-1" id="sec-30b5">
      <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-expanded-width u-tab-links-align-justify u-tabs u-tabs-1">
          <ul class="u-tab-list u-unstyled" role="tablist">
            <li class="u-tab-item" role="presentation">
              <a class="active u-active-white u-border-2 u-border-active-palette-2-base u-border-grey-15 u-border-hover-grey-15 u-border-no-bottom u-border-no-left u-border-no-right u-button-style u-grey-15 u-hover-grey-15 u-tab-link u-tab-link-1" id="link-tab-0da5" href="#tab-0da5" role="tab" aria-controls="tab-0da5" aria-selected="true">Графики работы</a>
            </li>
            <li class="u-tab-item" role="presentation">
              <a class="u-active-white u-border-2 u-border-active-palette-2-base u-border-grey-15 u-border-hover-grey-15 u-border-no-bottom u-border-no-left u-border-no-right u-button-style u-grey-15 u-hover-grey-15 u-tab-link u-tab-link-2" id="link-tab-14b7" href="#tab-14b7" role="tab" aria-controls="tab-14b7" aria-selected="false">Редактирование графиков</a>
            </li>
          </ul>
          <div class="u-tab-content">
            <div class="u-container-style u-tab-active u-tab-pane u-white u-tab-pane-1" id="tab-0da5" role="tabpanel" aria-labelledby="link-tab-0da5">
              <div class="u-container-layout u-container-layout-1">
                <h3 class="u-text u-text-default u-text-1">Выберете сотрудника</h3>
                <div class="u-form u-form-1">
                  <form action="" class="u-clearfix u-form-spacing-10 u-form-vertical u-inner-form" style="padding: 10px" source="custom" name="form">
                    <input type="hidden" id="siteId" name="siteId" value="126023929">
                    <input type="hidden" id="pageId" name="pageId" value="71664605">
                    <div class="u-form-group u-form-select u-form-group-1">
                      <label for="select-aefc" class="u-form-control-hidden u-label">Выбрать</label>
                      <div class="u-form-select-wrapper">
                        <select id="select-1" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
						<?php foreach ($finddoctors as $dfocs): ?>
                          <option value="<?php echo $dfocs['id'] ?>"><?php echo $dfocs['name'] ?></option>
                        <?php endforeach; ?>
                        </select>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
                      </div>
                    </div>
                    <div class="u-align-center u-form-group u-form-submit">
                      <a id="finddocrasp" href="" class="u-btn u-btn-submit u-button-style">Отправить</a>
                    </div>  
                  </form>
                </div>
                <div class="u-expanded-width u-table u-table-responsive u-table-1">
                  <table class="u-table-entity">
                    <colgroup>
                      <col width="33.3%">
                      <col width="33.3%">
                      <col width="33.400000000000006%">
                    </colgroup>
                    <thead class="u-align-center u-palette-1-base u-table-header u-table-header-1">
                      <tr style="height: 29px;">
                        <th class="u-table-cell">Сотрудник</th>
                        <th class="u-table-cell">Дата</th>
                        <th class="u-table-cell">Рабочие часы</th>
                      </tr>
                    </thead>
                    <tbody id="addnewrasp" class="u-align-center u-table-alt-palette-1-light-3 u-table-body">
					
                      
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="u-align-left u-container-style u-tab-pane u-white u-tab-pane-2" id="tab-14b7" role="tabpanel" aria-labelledby="link-tab-14b7">
              <div class="u-container-layout u-container-layout-2">
                <h3 class="u-text u-text-default u-text-2">Выберете сотрудника</h3>
                <div class="u-form u-form-2">
                  <form action="" class="u-clearfix u-form-spacing-10 u-form-vertical u-inner-form" style="padding: 10px" source="custom" name="form">
                    <div class="u-form-group u-form-select u-form-group-3">
                      <label for="select-aefc" class="u-form-control-hidden u-label">Выбрать</label>
                      <div class="u-form-select-wrapper">
                        <select id="select-2" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
                        <?php foreach ($finddoctors as $dfocs): ?>
                          <option value="<?php echo $dfocs['id'] ?>"><?php echo $dfocs['name'] ?></option>
                        <?php endforeach; ?>
                        </select>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
                      </div>
                    </div>
                    <div id="testcase" class="u-align-center u-form-group u-form-submit">
                      <a id="redrasp-btn" href="" class="u-btn u-btn-submit u-button-style">Отправить</a>
                      
                    </div>
                  </form>
                </div>
                <div class="u-expanded-width u-table u-table-responsive u-table-2">
                  <table class="u-table-entity">
                    <colgroup>
                      <col width="33.3%">
                      <col width="33.3%">
                      <col width="33.400000000000006%">
                    </colgroup>
                    <thead class="u-align-center u-palette-1-base u-table-header u-table-header-2">
                      <tr style="height: 29px;">
                        <th class="u-table-cell">Сотрудник</th>
                        <th class="u-table-cell">Дата</th>
                        <th class="u-table-cell">Рабочие часы</th>
                      </tr>
                    </thead>
                    <tbody id="addnewraspred" class="u-align-center u-table-alt-palette-1-light-3 u-table-body">
					
                     <!-- <tr style="height: 76px;">
                        <td class="u-table-cell">---</td>
                        <td class="u-table-cell">---</td>
                        <td class="u-table-cell">
						
						<div class="u-form-select-wrapper">
                        <select id="select-91ac" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
                          <option value="1">От</option>
						  <option value="10">10:00</option>
                          <option value="11">11:00</option>
						  <option value="12">12:00</option>
						  <option value="13">13:00</option>
						  <option value="14">14:00</option>
						  <option value="15">15:00</option>
						  <option value="16">16:00</option>
						  <option value="17">17:00</option>
						  <option value="18">18:00</option>
						  <option value="19">19:00</option>
						  <option value="20">20:00</option>
						  <option value="21">21:00</option>
                        </select>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
                        </div>
						<div class="u-form-select-wrapper">
                        <select id="select-93ac" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
                          <option value="1">До</option>
                          <option value="11">11:00</option>
						  <option value="12">12:00</option>
						  <option value="13">13:00</option>
						  <option value="14">14:00</option>
						  <option value="15">15:00</option>
						  <option value="16">16:00</option>
						  <option value="17">17:00</option>
						  <option value="18">18:00</option>
						  <option value="19">19:00</option>
						  <option value="20">20:00</option>
						  <option value="21">21:00</option>
						  <option value="22">22:00</option>
                        </select>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
                        </div>
						
						</td>
                      </tr> -->
                      
                    </tbody>
                  </table>
                </div>
                <center>
                <a id="saverasp-btn" class="u-border-2 u-border-hover-palette-1-base u-border-palette-1-base u-btn u-btn-round u-button-style u-hover-palette-1-base u-none u-radius-50 u-btn-4">Сохранить</a>
                </center>
			  </div>
            </div>
          </div>
        </div>
      </div>
    </section>