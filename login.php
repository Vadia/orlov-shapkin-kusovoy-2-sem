<?php
require 'php/db/db.php';
$data = $_POST;
$mess = 0;

if(isset($_SESSION['logged_user'])):
header('Location: lk.php');
else:

if ( isset($data['btn']) )
	{
$user = R::findOne('users', 'login = ?', array($data['login']));

        if ( $user ) //логин существует
		{
			if ( password_verify($data['pass'], $user->pass) )
			{
				
				//$user->lastlogin = date('Ymd');
				//R::store($user);
				//если пароль совпадает, то нужно авторизовать пользователя
				$_SESSION['logged_user'] = $user;
				
				
				$sess = R::dispense('authsess');
				$sess->login = $_SESSION['logged_user']->login;
				$sess->ip = $_SERVER['REMOTE_ADDR'];
				$sess->grp = $_SESSION['logged_user']->grp;
				$sess->datetime = Date("Y-m-d / H:i:s");
				R::store($sess);
				header('Location: lk.php');
				
			}else{
			$errors[] = 'Неверный пароль!';
			$mess = 1;
				
			}
			
		}else{
			$errors[] = 'Пользователь с таким логином не найден!';
			$mess = 1;
		}
	}
endif;
 ?>
<html>
<head>
<title>Авторизация</title>
<link rel="stylesheet" href="assets/css/app.css">
<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
</head>
<body class="light">
<div id="primary" class="p-t-b-100 height-full ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto">
                    <div class="text-center">
                        <img src="assets/img/u1.png" alt="">
                        <h3 class="mt-2">Добро пожаловать</h3>
						
						<?php 	if ( $mess == 1) : ?>
					<?php echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div>';  ?>
						<?php endif;?>
						
						<hr color="black" width="300">
                       <a href="/reg.php" ><p class="p-t-b-20">Если у вас нет учетной записи, предлагаем вам зарегистрироваться!</p></a>
                    </div>
                    <form method="POST">
                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                            <input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control form-control-lg"
                                   placeholder="Логин">
                        </div>
                        <div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input type="password" name="pass" value="<?php echo @$data['pass']; ?>" class="form-control form-control-lg"
                                   placeholder="Пароль">
                        </div>
                        <input type="submit" name="btn" class="btn btn-success btn-lg btn-block">
                       <a href="/recovery.php"><p class="forget-pass">Забыли логин или пароль?</p></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>	
</html>