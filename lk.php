<?php
require 'php/db/db.php';
$rasp = $_GET['raspdok'];
$vsezayavki = $_GET['vsezayavki'];
$raspredactor = $_GET['raspredactor'];
$mypet = $_GET['mypet'];
$mydoctor = $_GET['mydoctor'];
$pasients = $_GET['pasients'];
$dash = $_GET['dashboard'];
$raspisdoc = $_GET['raspisdoc'];
 ?>
<?php if(isset ($_SESSION['logged_user'])): ?>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <meta name="description" content="">
    
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/gijgo.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/slicknav.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
	
	<!-- линки для dashboard php -->
  <link rel="stylesheet" href="pages/dashboard/vendors/feather/feather.css">
  <link rel="stylesheet" href="pages/dashboard/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="pages/dashboard/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="pages/dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="pages/dashboard/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" type="text/css" href="pages/dashboard/js/select.dataTables.min.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="pages/dashboard/css/vertical-layout-light/style.css">
	
	
	<!-- линки для listzapis php -->
	    <link rel="stylesheet" href="pages/write/assets/np.css" media="screen">
    <link rel="stylesheet" href="pages/todo/adminmoder.css" media="screen">
    <!-- <script class="u-script" type="text/javascript" src="pages/write/assets/jquery.js" defer=""></script> -->
    <script class="u-script" type="text/javascript" src="pages/write/assets/np.js" defer=""></script>
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    
</head>

<body>
 
    

    <header>
        <div class="header-area ">
            <div class="header-top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#">+7 (999)999-99-99</a></li>
                                    <li><a href="#">Пн-Сб 8:00 - 22:00</a></li>
									
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 ">
                            <div class="social_media_links">
                                <a href="#">
								<?php echo $_SESSION['logged_user']->name; ?>
                                </a>
								<?php if($_SESSION['logged_user']->grp == "admin"):?>
								<a href="lk.php?dashboard=1">
								Доска
                                </a>
								<?php endif; ?>
                                <a href="/logout.php">
                                    Выйти
                                </a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-3">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="assets/img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a  href="index.php">Домой</a></li>
										<?php if($_SESSION['logged_user']->grp == "doctor"): ?>
										<li><a  href="lk.php">Личный кабинет</a></li>
                                        <li><a href="/lk.php?raspdok=1">Запись</a></li>
										<li><a href="/lk.php?raspisdoc=1">Расписание</a></li>
                                        <li><a href="/lk.php?pasients=1">Мои пациенты</a></li>
										<?php elseif($_SESSION['logged_user']->grp == "admin"): ?>
										<li><a  href="lk.php">Личный кабинет</a></li>
                                        <li><a href="/lk.php?raspdok=1">Новые заявки</a></li>
										<li><a href="/lk.php?raspredactor=1">Расписания</a></li>										
										<li><a href="/lk.php?pasients=1">Список пациентов</a></li>
										<?php elseif($_SESSION['logged_user']->grp == "moder"): ?>
										<li><a  href="lk.php">Личный кабинет</a></li>
                                        <li><a href="/lk.php?raspdok=1">Новые заявки</a></li>
										<li><a href="/lk.php?raspredactor=1">Расписания</a></li> 
										<li><a href="/lk.php?pasients=1">Список пациентов</a></li>
										<?php elseif($_SESSION['logged_user']->grp == "user"): ?>
										<li><a  href="lk.php">Личный кабинет</a></li>
                                        <li><a href="/lk.php?mypet=1">Мой питомец</a></li>
										<li><a href="/lk.php?mydoctor=1">Мой доктор</a></li>
										<?php endif; ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- slider_area_start -->
    
    <!-- slider_area_end -->

    <!-- service_area_start  -->
<?php if(isset($raspisdoc)): include_once 'pages/raspdoc.php'; ?>
<?php else: ?>
<?php if(isset($dash)): include_once 'pages/dashboard/dashboard.php'; ?>
<?php else: ?>
<?php if(isset($mypet)): include_once 'pages/mypet.php'; ?>
<?php else: ?>
<?php if(isset($mydoctor)): include_once 'pages/mydoctor.php'; ?>
<?php else: ?>
<?php if(isset($pasients)): include_once 'pages/pasients.php'; ?>
<?php else: ?>
<?php if(isset($raspredactor)): include_once 'pages/raspredactor.php'; ?>
<?php else: ?>
<?php if(isset($vsezayavki)): ?>
<?php if($_SESSION['logged_user']->grp == "admin" OR $_SESSION['logged_user']->grp == "moder"): ?>
<div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-7 col-md-10">
				
                    <div class="section_title text-center mb-95">
                        <h3 class="statusadm" name="1">Все заявки</h3>
                    </div>	
					
					<div class="section_title text-center mb-95">
					<div class="single-element-widget mt-30">
					<div id="adminoshibka"></div>
							<h2 class="mb-30">Выберете день</h2>
							<div class="default-select" id="default-select">
										<input id="selectdatevsezayavki" type="date" name="calendar">
							</div><br>
							<a id="selectdayvsezayavki" class="genric-btn primary-border">Показать</a>
					</div>
					</div>
					
					<div class="section_title text-center mb-95">
                     <div id="vsezayavki" class="section_title text-center mb-95">
                                            <table class="table">
                                                <thead>
                                                    <tr>
														<th scope="col">Время</th>
														<th scope="col">Доктор</th>
                                                        <th scope="col">Цена</th>
														<th scope="col">Имя</th>
														<th scope="col">Номер</th>
														<th scope="col">Действия</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="contentvsezayavki">
                                                </tbody>
                                            </table>
                     </div>   
                    </div>
					
			    </div>
            </div>
</div>
<?php endif; ?>
<?php else: ?>
<?php if(isset($rasp)): ?>
        <div id="delall" class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-7 col-md-10">
				    <?php if($_SESSION['logged_user']->grp == "doctor"): ?>
                    <div class="section_title text-center mb-95">
                        <h2 class="statusadm" name="1">Запись</h2>
                    </div>
					
                    <center>
					<div class="single-element-widget mt-30">
					<div id="doctoroshibka"></div>
							<h3 class="mb-30">Выберете день</h3>
							<div class="default-select" id="default-select">
										<input id="doctorselectday" type="date" name="calendar">
							</div><br>
							<a id="btnday" class="genric-btn primary-border">Показать</a>
					</div></center><br>
					
					
					<div id="raspcontent" class="section_title text-center mb-95">
                                            <table class="table">
                                                <thead>
                                                    <tr>
														<th scope="col">Время</th>
														<th scope="col">Доктор</th>
                                                        <th scope="col">Цена</th>
														<th scope="col">Имя</th>
														<th scope="col">Номер</th>
														<th scope="col">Статус</th>
														<th scope="col">Действия</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="doctortime">
                                                </tbody>
                                            </table>
                    </div>
				<?php elseif($_SESSION['logged_user']->grp == "admin" OR $_SESSION['logged_user']->grp == "moder"): ?>
				<div class="section_title text-center mb-95">
					    <h2 class="statusadm" name="0">Заявки от клиентов</h2>
				</div>
				
				<center>
					<div class="single-element-widget mt-30">
					<div id="doctoroshibka"></div>
							<h3 class="mb-30">Выберете день</h3>
							<div class="default-select" id="default-select">
										<input id="doctorselectday" type="date" name="calendar">
							</div><br>
							<a id="btnday" class="genric-btn primary-border">Показать</a>
					</div></center><br>
					
					
					<div id="raspcontent" class="section_title text-center mb-95">
                                            <table class="table">
                                                <thead>
                                                    <tr>
														<th scope="col">Время</th>
														<th scope="col">Доктор</th>
                                                        <th scope="col">Цена</th>
														<th scope="col">Имя</th>
														<th scope="col">Номер</th>
														<th scope="col">Действия</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="doctortime">
                                                </tbody>
                                            </table>
                    </div>
					
			    <?php endif; ?>	
					
					
                </div>
            </div>
        </div>	
<?php else: ?>


                      <?php if($_SESSION['logged_user']->grp == "admin" OR $_SESSION['logged_user']->grp == "moder"): ?>
					
                       <!-- <h3>Личный кабинет администратора</h3> -->
						<?php include_once 'pages/todo/listzapis.php'; ?>
	
	
	
	
    <div class="service_area">
        <div class="container">
            
                
                    
					<?php elseif($_SESSION['logged_user']->grp == "doctor"): include_once 'pages/lkdoctor.php'; ?>
					

					<div class="row justify-content-center ">
					<div class="col-lg-7 col-md-10">
                    <div class="section_title text-center mb-95">  
					

                    <?php elseif($_SESSION['logged_user']->grp == "user"): ?>
					<div class="section_title text-center mb-95"> 
                        <h3>Личный кабинет клиента</h3>	
                    </div>						
					<?php endif; ?>	
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php endif; ?>
<?php endif; ?> 
<?php endif; ?> 
<?php endif; ?>   
<?php endif; ?>
<?php endif; ?>  
<?php endif; ?> 
<?php endif; ?> 

    <!-- footer_start  -->
    <footer class="footer">
        
        <div class="copy-right_text">
            <div class="container">
                <div class="bordered_1px"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
  <p>
  Copyright &copy; 2021 - <?php echo Date('Y'); ?> Все права защищены | by <a href="" target="_blank">ORLOV | SHAPKIN</a>
  </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer_end  -->


    <!-- JS here -->
    <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/ajax-form.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/scrollIt.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/nice-select.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/gijgo.min.js"></script>

    <!--contact js-->
    <script src="assets/js/contact.js"></script> 
    <script src="assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="assets/js/jquery.form.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/mail-script.js"></script>

    <script src="assets/js/main.js"></script>
	<script src="assets/ajax/ajax.js"></script>
	<script src="assets/ajax/pets.js"></script>
	<script src="assets/ajax/jquery.slimscroll.min.js"></script>
	
	<script src="pages/todo/todo.js"></script>
    
</body>

</html>

<?php else : header('Location: /login.php'); ?>


<?php endif; ?>