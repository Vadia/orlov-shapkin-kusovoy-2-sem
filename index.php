<?php


require 'php/db/db.php';

$gowrite = $_GET['gowrite'];
$contact = $_GET['contact'];
$blog = $_GET['blog'];
 ?>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>VetClinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/gijgo.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/slicknav.css">
    <link rel="stylesheet" href="assets/css/style.css">
	
	<!-- Для модуля записи -->
	
	<link rel="stylesheet" href="pages/write/assets/np.css" media="screen">
    <link rel="stylesheet" href="pages/write/assets/Главная.css" media="screen">
    <!-- <script class="u-script" type="text/javascript" src="pages/write/assets/jquery.js" defer=""></script> -->
    <script class="u-script" type="text/javascript" src="pages/write/assets/np.js" defer=""></script>
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700">
    
</head>

<!-- старый вариант <?php include_once 'pages/formindex.php'; ?> -->

 <?php if($gowrite == 1) : include_once 'pages/write/fastwrite.php';  ?>
<?php else: ?>
<body>
    <header>
        <div id="noscroll" class="header-area ">
            <div class="header-top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#">+7 (999)999-99-99</a></li>
                                    <li><a href="#">Пн-Сб 8:00 - 22:00</a></li>
									<?php if(isset($_SESSION['logged_user'])): ?>
									<li><a href="/lk.php">ЛК</a></li>
									<?php else: ?>
									<li><a href="/login.php">Вход</a></li>
									<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-4 ">
                            <div class="social_media_links">
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-pinterest-p"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-3">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="assets/img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a  href="index.php">Домой</a></li>
										<li><a href="?blog=1">Блог</a></li>
                                        <li><a href="?contact=1">Контакты</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
	
	<?php if(isset($contact)): include_once 'pages/contact.php'; ?>
	<?php else: ?>
	<?php if(isset($blog)): include_once 'pages/blog.php'; ?>
	<?php else: ?>
    
    <div class="slider_area">
        <div class="single_slider slider_bg_1 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="slider_text">
                            <h3>Мы любим <br> <span>Ваших друзей</span></h3>
                            <br>
                            <a href="?gowrite=1" class="boxed-btn4">Записаться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dog_thumb d-none d-lg-block">
                <img src="assets/img/banner/dog.png" alt="">
            </div>
        </div>
    </div>
    
    <div class="service_area">
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-7 col-md-10">
                    <div class="section_title text-center mb-95">
                        <h3>Сервис для любимых питомцев</h3>
                        
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="assets/img/service/service_icon_1.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>Передержка животных</h3>
                            
                         </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_service active">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="assets/img/service/service_icon_2.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>Медицинская помощь</h3>
                            
                         </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="assets/img/service/service_icon_3.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>СПА для животных</h3>
                            
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="pet_care_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6">
                    <div class="pet_thumb">
                        <img src="assets/img/about/pet_care.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-6">
                    <div class="pet_info">
                        <div class="section_title">
                            <h3><span>Мы заботимся о ваших питомцах </span> <br>
							Как о вас</h3>
                            <p>Мы проявляем к вашим питомцам такую же заботу какую проявляем к вам. Ваше желание и желание вашего питомца для нас закон </p>
                            <a href="?gowrite=1" class="boxed-btn3">Записаться</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="adapt_area">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-5">
                    <div class="adapt_help">
                        <div class="section_title">
                            <h3><span>Нам нужна ваша</span>
							помощь Примите нас</h3>
                            <p>Множество животных в данный момент нуждаются в хозяевах. Вы можете помочь им взяв их к себе</p>
                            <a href="?gowrite=1" class="boxed-btn3">Записаться</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="adapt_about">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <div class="single_adapt text-center">
                                    <img src="assets/img/adapt_icon/1.png" alt="">
                                    <div class="adapt_content">
                                        <h3 class="counter">14</h3>
                                        <p>Больных собак</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="single_adapt text-center">
                                    <img src="assets/img/adapt_icon/3.png" alt="">
                                    <div class="adapt_content">
                                        <h3><span class="counter">7</span>+</h3>
                                        <p>Собак</p>
                                    </div>
                                </div>
                                <div class="single_adapt text-center">
                                    <img src="assets/img/adapt_icon/2.png" alt="">
                                    <div class="adapt_content">
                                        <h3><span class="counter">19</span>+</h3>
                                        <p>Других животных</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <div class="testmonial_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="textmonial_active owl-carousel">
                        <div class="testmonial_wrap">
                            <div class="single_testmonial d-flex align-items-center">
                                <div class="test_thumb">
                                    <img src="assets/img/testmonial/1.png" alt="">
                                </div>
                                <div class="test_content">
                                    <h4>Валентин Федоров</h4>
                                    <span>Сантехник</span>
                                    <p>Лучшая клиника, лучшие врачи</p>
                                </div>
                            </div>
                        </div>
                        <div class="testmonial_wrap">
                            <div class="single_testmonial d-flex align-items-center">
                                <div class="test_thumb">
                                    <img src="assets/img/testmonial/1.png" alt="">
                                </div>
                                <div class="test_content">
                                    <h4>Андрей Васечкин</h4>
                                    <span>Столяр</span>
                                    <p>Взял собачку из этой клиники. Теперь это мой любимый питомец</p>
                                </div>
                            </div>
                        </div>
                        <div class="testmonial_wrap">
                            <div class="single_testmonial d-flex align-items-center">
                                <div class="test_thumb">
                                    <img src="assets/img/testmonial/1.png" alt="">
                                </div>
                                <div class="test_content">
                                    <h4>Светлана Иванова</h4>
                                    <span>Художник</span>
                                    <p>Помогли мне вылечить моего питомца. Спасибо!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div class="team_area">
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-6 col-md-10">
                    <div class="section_title text-center mb-95">
                        <h3>Наша команда</h3>
                        <p>Мы - собрание не равнодушных к животным людей</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="single_team">
                        <div class="thumb">
                            <img src="assets/img/team/1.png" alt="">
                        </div>
                        <div class="member_name text-center">
                            <div class="mamber_inner">
                                <h4>Наташа Швецова</h4>
                                <p>Генеральный директор</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_team">
                        <div class="thumb">
                            <img src="assets/img/team/2.png" alt="">
                        </div>
                        <div class="member_name text-center">
                            <div class="mamber_inner">
                                <h4>Артур Даниилов</h4>
                                <p>Менеджер</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_team">
                        <div class="thumb">
                            <img src="assets/img/team/3.png" alt="">
                        </div>
                        <div class="member_name text-center">
                            <div class="mamber_inner">
                                <h4>Лариса Катушкина</h4>
                                <p>Главврач</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <div class="contact_anipat anipat_bg_1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="contact_text text-center">
                        <div class="section_title text-center">
                            <h3>Почему стоит работать с Vet Clinic</h3>
                            <p>Потому что мы знаем, что даже лучшая технология хороша не настолько, насколько хороши люди, стоящие за ней. 24/7 техподдержка.</p>
                        </div>
                        <div class="contact_btn d-flex align-items-center justify-content-center">
                            <a href="?gowrite=1" class="boxed-btn4">Запишитесь</a>
                            <p>Или  <a href=""> +79524524932</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Связаться с нами
                            </h3>
                            <ul class="address_line">
                                <li>+79524524932</li>
                                <li><a href="#">vetclinic@gmail.Com</a></li>
                                <li>Москва, улица Кащенко д.4 кв.9</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3  col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Наши услуги
                            </h3>
                            <ul class="links">
                                <li><a href="#">Страхование домашних животных</a></li>
                                <li><a href="#">Операции для домашних животных </a></li>
                                <li><a href="#">Прием к себе домашних животных</a></li>
                                <li><a href="#">Страхование собак</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3  col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Быстрые ссылки
                            </h3>
                            <ul class="links">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">Политика конфиденциальности</a></li>
                                <li><a href="#">Условия использования</a></li>
                                <li><a href="#">Информация для входа</a></li>
                                <li><a href="#">База знаний</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3 ">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="assets/img/logo.png" alt="">
                                </a>
                            </div>
                            <p class="address_text">Москва, улица Кащенко д.4 кв.9
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php endif; ?>	<?php endif; ?>	
		
        <div class="copy-right_text">
            <div class="container">
                <div class="bordered_1px"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <p>
   &copy; 2021 - <?php echo Date('Y'); ?> Все права защищены | by <a href="" target="_blank">Orlov|Shapkin</a>
  </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    


    
    <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/ajax-form.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/scrollIt.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/nice-select.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/gijgo.min.js"></script>

   
    

    <script src="assets/js/main.js"></script>
	
	<script src="assets/ajax/ajax.js"></script>
	<script src="assets/ajax/jquery.slimscroll.min.js"></script>
	
	
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            disableDaysOfWeek: [0, 0],

        });
        $('#datepicker2').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }

        });
        var timepicker = $('#timepicker').timepicker({
         format: 'HH.MM'
     });
    </script>
</body>
<?php endif; ?>	



</html>