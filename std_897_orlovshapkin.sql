-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: std-mysql
-- Время создания: Июл 09 2021 г., 16:12
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `std_897_orlovshapkin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `srcimg` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `vives` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `date`, `srcimg`, `category`, `text`, `vives`) VALUES
(1, 'Как нужно ухаживать за кошкой?', '2021-07-09', 'pages/blogimg/img1.jpg', 'Кошки', '1\r\n2\r\n3\r\n', 3),
(2, 'Морская свинка, особенности питания', '2021-07-06', 'pages/blogimg/img2.jpg', 'Морские свинки', 'Завтрак, обед, ужин!', NULL),
(3, 'Дрессировка собаки, тонкости.', '2021-07-06', 'pages/blogimg/img3.jpg', 'Собаки', 'Дрессировка - дело непростое!', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `opis` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `doctors`
--

INSERT INTO `doctors` (`id`, `login`, `name`, `opis`) VALUES
(1, 'vadia', 'Андрей Александрович Петров', 'В 2009 году окончил факультет ветеринарной медицины Московской государственной академии ветеринарной медицины и биотехнологии им. К.И. Скрябина. В 2013 году защитил кандидатскую диссертацию. Старший преподаватель на кафедре морфологии и ветеринарии РГАУ-МСХА имени К.А. Тимирязева с 2015 года.');

-- --------------------------------------------------------

--
-- Структура таблицы `messagesfromcontact`
--

CREATE TABLE `messagesfromcontact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messagesfromcontact`
--

INSERT INTO `messagesfromcontact` (`id`, `name`, `email`, `text`) VALUES
(3, 'Алексей', 'vag.rdwevgt@yandex.ru', 'Хочу записать собаку в спа!'),
(4, 'Михаил', 'ksrklsplrgoturfhpl@mail.ru', 'Здравствуйте!');

-- --------------------------------------------------------

--
-- Структура таблицы `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `owner` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `opis` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pets`
--

INSERT INTO `pets` (`id`, `name`, `owner`, `opis`, `type`) VALUES
(2, 'Кира', 'user', 'Хорошая кошка', 'Кошка');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pass` varchar(600) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `namepet` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `changepass` int(5) DEFAULT NULL,
  `grp` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `doclogin` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pet` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `pass`, `name`, `namepet`, `changepass`, `grp`, `doclogin`, `phone`, `pet`) VALUES
(4, 'vadia', 'vad.orov@gmail.com', '$2y$10$.XL55T6YgAy3tbj6pI/yse1UHYa/1GIoZmWfs5pkwSmOycRuQBgtK', 'vadia', 'Барсик', NULL, 'doctor', NULL, NULL, NULL),
(5, 'admin', 'vadd.orov@gmail.com', '$2y$10$o0vYNPRAa/NOf46o5oUmu.0/Hc7QoKU1aMCWspXoavyx5X..FMF56', 'Александр', '+', NULL, 'admin', NULL, NULL, NULL),
(6, 'user', 'vadia.orlov2015@yandex.ru', '$2y$10$o1ZJgpeTChV/dV.K/weJXO4sxIYbAxoprAUDQP9f92OsAzRINWiEC', 'Андрей', 'Кира', NULL, 'user', 'vadia', '+79938434234', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `zapis`
--

CREATE TABLE `zapis` (
  `id` int(11) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `doctor` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `orderby` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zapis`
--

INSERT INTO `zapis` (`id`, `login`, `date`, `time`, `doctor`, `price`, `name`, `phone`, `status`, `orderby`) VALUES
(22, NULL, '2021-07-07', '8:00', 'vadia', '500р', 'Саша', '+79938434234', 3, 800),
(23, NULL, '2021-07-07', '9:00', 'vadia', '700р', 'Андрей', '+798432', 3, 900),
(24, NULL, '2021-07-09', '8:00', 'Логин доктора', 'Цена', 'Саша', '+79938434234', 0, 800);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messagesfromcontact`
--
ALTER TABLE `messagesfromcontact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `zapis`
--
ALTER TABLE `zapis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `messagesfromcontact`
--
ALTER TABLE `messagesfromcontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `zapis`
--
ALTER TABLE `zapis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
