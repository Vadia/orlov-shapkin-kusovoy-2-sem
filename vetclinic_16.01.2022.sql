-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: std-mysql
-- Время создания: Янв 16 2022 г., 17:08
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `std_897_orlovshapkin`
--
CREATE DATABASE IF NOT EXISTS `std_897_orlovshapkin` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `std_897_orlovshapkin`;

-- --------------------------------------------------------

--
-- Структура таблицы `authsess`
--

CREATE TABLE `authsess` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` varchar(191) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `authsess`
--

INSERT INTO `authsess` (`id`, `login`, `ip`, `grp`, `datetime`) VALUES
(3, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:13:15'),
(4, 'moder', '127.0.0.1', 'moder', '2022-01-15 / 19:13:30'),
(5, 'vadia', '127.0.0.1', 'doctor', '2022-01-15 / 19:13:42'),
(6, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:14:02'),
(7, 'moder', '127.0.0.1', 'moder', '2022-01-15 / 19:46:23'),
(8, 'moder', '127.0.0.1', 'moder', '2022-01-15 / 19:46:30'),
(9, 'moder', '127.0.0.1', 'moder', '2022-01-15 / 19:46:36'),
(10, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:46:42'),
(11, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:46:48'),
(12, 'vadia', '127.0.0.1', 'doctor', '2022-01-15 / 19:47:00'),
(13, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:47:08'),
(14, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:48:19'),
(15, 'vadia', '127.0.0.1', 'doctor', '2022-01-15 / 19:48:41'),
(16, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:48:57'),
(17, 'vadia', '127.0.0.1', 'doctor', '2022-01-15 / 19:53:10'),
(18, 'admin', '127.0.0.1', 'admin', '2022-01-15 / 19:53:34'),
(19, 'admin', '127.0.0.1', 'admin', '2022-01-16 / 18:33:48');

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `srcimg` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `vives` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `date`, `srcimg`, `category`, `text`, `vives`) VALUES
(1, 'Как нужно ухаживать за кошкой?', '2021-07-09', 'pages/blogimg/img1.jpg', 'Кошки', '1\r\n2\r\n3\r\n', 3),
(2, 'Морская свинка, особенности питания', '2021-07-06', 'pages/blogimg/img2.jpg', 'Морские свинки', 'Завтрак, обед, ужин!', NULL),
(3, 'Дрессировка собаки, тонкости.', '2021-07-06', 'pages/blogimg/img3.jpg', 'Собаки', 'Дрессировка - дело непростое!', NULL),
(4, 'Кот не спит ночью!', '2021-07-06', 'pages/blogimg/img3.jpg', 'Кошки', 'Намочите ему хвост!', NULL),
(5, 'Как искупать собаку', '2021-06-31', 'pages/blogimg/img3.jpg', 'Собаки', 'В ванной - легко! нужно просто ...', NULL),
(6, 'Морская свинка не кушает! Что делать?', '2021-07-05', 'pages/blogimg/img2.jpg', 'Морские свинки', 'Нужно наполнить ее тарелку и она будет кушать!', NULL),
(7, 'Как помочь собаке при родах?', '2021-07-03', 'pages/blogimg/img3.jpg', 'Собаки', 'Теплая вода\r\nУютное место и ваша собака справится с этой нелегкой задачей!', NULL),
(8, 'Уход за хомяком', '2021-07-01', 'pages/blogimg/img3.jpg', 'Хомяки', 'Не забывать поить и кормить его!', NULL),
(9, 'Как выбрать щенка?', '2021-07-06', 'pages/blogimg/img3.jpg', 'Собаки', 'Выбирайте самого крепкого и игривого!', NULL),
(10, 'Покупка шиншиллы', '2021-06-31', 'pages/blogimg/img3.jpg', 'Шиншиллы', 'Лучше не покупать! столько бед от этого животного', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `DoctorRasp`
--

CREATE TABLE `DoctorRasp` (
  `id` int(11) NOT NULL,
  `doctor` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `doctorid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `DoctorRasp`
--

INSERT INTO `DoctorRasp` (`id`, `doctor`, `date`, `time`, `doctorid`) VALUES
(1, 'Бялко Г.И.', '2022-01-11', '11:00', '4'),
(2, 'Бялко Г.И.', '2022-01-11', '12:00', '4'),
(4, 'Бялко Г.И.', '2022-01-12', '11:00', '4'),
(10, 'Бялко Г.И.', '2022-01-12', '12:00', '4'),
(11, 'Бялко Г.И.', '2022-01-12', '13:00', '4'),
(12, 'Бялко Г.И.', '2022-01-12', '14:00', '4'),
(18, 'Бялко Г.И.', '2022-01-13', '10:00', '4'),
(19, 'Бялко Г.И.', '2022-01-13', '11:00', '4'),
(20, 'Бялко Г.И.', '2022-01-13', '12:00', '4'),
(21, 'Бялко Г.И.', '2022-01-13', '13:00', '4'),
(22, 'Бялко Г.И.', '2022-01-13', '14:00', '4'),
(23, 'Бялко Г.И.', '2022-01-13', '15:00', '4'),
(24, 'Бялко Г.И.', '2022-01-14', '10:00', '4'),
(25, 'Бялко Г.И.', '2022-01-14', '11:00', '4'),
(26, 'Бялко Г.И.', '2022-01-14', '12:00', '4'),
(192, 'Бялко Г.И.', '2022-01-16', '10:00', '4'),
(193, 'Бялко Г.И.', '2022-01-16', '11:00', '4'),
(194, 'Бялко Г.И.', '2022-01-16', '12:00', '4'),
(195, 'Бялко Г.И.', '2022-01-17', '10:00', '4'),
(196, 'Бялко Г.И.', '2022-01-17', '11:00', '4'),
(197, 'Бялко Г.И.', '2022-01-17', '12:00', '4'),
(198, 'Бялко Г.И.', '2022-01-18', '10:00', '4'),
(199, 'Бялко Г.И.', '2022-01-18', '11:00', '4'),
(200, 'Бялко Г.И.', '2022-01-18', '12:00', '4'),
(201, 'Бялко Г.И.', '2022-01-19', '10:00', '4'),
(202, 'Бялко Г.И.', '2022-01-19', '11:00', '4'),
(203, 'Бялко Г.И.', '2022-01-19', '12:00', '4'),
(204, 'Бялко Г.И.', '2022-01-20', '10:00', '4'),
(205, 'Бялко Г.И.', '2022-01-20', '11:00', '4'),
(206, 'Бялко Г.И.', '2022-01-20', '12:00', '4'),
(207, 'Бялко Г.И.', '2022-01-21', '10:00', '4'),
(208, 'Бялко Г.И.', '2022-01-21', '11:00', '4'),
(209, 'Бялко Г.И.', '2022-01-21', '12:00', '4'),
(210, 'Бялко Г.И.', '2022-01-22', '10:00', '4'),
(211, 'Бялко Г.И.', '2022-01-22', '11:00', '4'),
(212, 'Бялко Г.И.', '2022-01-22', '12:00', '4');

-- --------------------------------------------------------

--
-- Структура таблицы `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `opis` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `doctors`
--

INSERT INTO `doctors` (`id`, `login`, `name`, `opis`) VALUES
(1, 'vadia', 'Геннадий Иванович Бялко', 'В 2009 году окончил факультет ветеринарной медицины Московской государственной академии ветеринарной медицины и биотехнологии им. К.И. Скрябина. В 2013 году защитил кандидатскую диссертацию. Старший преподаватель на кафедре морфологии и ветеринарии РГАУ-МСХА имени К.А. Тимирязева с 2015 года.'),
(4, 'Sasha123', 'Олег Николаевич Сурков', 'Лучший специалист в своем деле...'),
(5, 'Orsdimd', 'Олег Андреевич Бурденко', 'Главный врач..'),
(12, 'fefefef', 'Олег Андреевич Бурденко', 'Главный врач..'),
(13, 'effOefrsdimd', 'Сергей Дмитриевич', 'Доктор с большим стажем работы..'),
(14, 'efefOrfesdimd', 'Вячеслав Алесандрович', 'Специалист по уходу за ..'),
(15, 'eefOrsdfimd', 'Алексей Олегович', 'Главный онколог..'),
(16, '5fOfefrsdimd', 'Игорь Авраалович', 'Специалист станции переливания крови ..'),
(17, '3ffdOr3fsdimd', 'Андрей Анатольевич Кубаров', 'Анастезиолог-реаниматолог ..'),
(18, '3fO3frsdimd', 'Григорий Владиславович Пупков', 'Практикант'),
(19, 'f33Or3f3fsdimd', 'Сергей Сергеевич Мальков', 'Глазной хирург'),
(20, 'f33Orf3sdfif3d', 'Клим Иванович Метела', 'Главный врач отделения гнойной хирургии');

-- --------------------------------------------------------

--
-- Структура таблицы `messagesfromcontact`
--

CREATE TABLE `messagesfromcontact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messagesfromcontact`
--

INSERT INTO `messagesfromcontact` (`id`, `name`, `email`, `text`) VALUES
(3, 'Алексей', 'vag.rdwevgt@yandex.ru', 'Хочу записать собаку в спа!'),
(4, 'Михаил', 'ksrklsplrgoturfhpl@mail.ru', 'Здравствуйте!'),
(5, 'Владислав', '+++@mail.ru', 'Всем привет!!!'),
(6, 'Евгений', 'vadia.orlov2015@yandex.ru', 'Добрый вечер!'),
(7, 'Андрей', 'djfkjdfj@mail.ru', 'Как вы?'),
(8, 'Андрей', 'djfkjdfj@mail.ru', 'Как можно записаться на прием??'),
(9, 'Андрей', 'djfkjdfj@mail.ru', 'У меня не получается! Что делать? Подскажите!!!'),
(10, 'Анна', 'у3к1dj2fkjd342fj@mail.ru', 'Здравствуйте, я из компании орифлейм'),
(11, 'Анна', 'у3к1dj2fkjd342fj@mail.ru', 'Хочу продать вам как можно больше товаров! Вы не против?'),
(12, 'Саша', 'ihrogkoeko@gmail.com', 'Помогите! У меня слетела запись!'),
(13, 'Саша', 'ihrogkoeko@gmail.com', 'Я прошу вас! мне нужно попасть вовремя к моему доктору! Собака сильно болеет Чихает, кашляет!'),
(14, 'Усуп', 'kdhgjodf@mail.ru', 'Куплю кота любой породы! бюджет - 100р');

-- --------------------------------------------------------

--
-- Структура таблицы `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `owner` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `opis` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pets`
--

INSERT INTO `pets` (`id`, `name`, `owner`, `opis`, `type`) VALUES
(2, 'Котик', 'user', 'Добрый кот!!', 'Кот'),
(3, 'Пушок', 'skdnfs', '+', 'Кот'),
(4, 'Пушок', 'skdnfs', '+', 'Кот'),
(5, 'Ветерок', 'fggffgh', 'Хороший кот добрый', 'Кот'),
(6, 'Мальма', 'thhth', '+', 'Собака'),
(7, 'Иван', 'nubtrv', '+', 'Енот'),
(8, 'Кампотик', 'eggr5', '+', 'Скунс'),
(9, 'Зайка', 'hmhbvrv2', 'Приболевшая улитка', 'Улитка'),
(10, 'Кира', 'nhgbvdv8', '+', 'Кот'),
(11, 'Лопух', 'svffvre', '+', 'Кот'),
(12, 'Юпитер', 'deehgdfs', 'Здоровый пес-кабан', 'Собака'),
(13, 'Мухтарчик', 'gbnibh2443', '+', 'Собака');

-- --------------------------------------------------------

--
-- Структура таблицы `todolist`
--

CREATE TABLE `todolist` (
  `id` int(11) NOT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `checkbox` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `todolist`
--

INSERT INTO `todolist` (`id`, `text`, `checkbox`) VALUES
(3, 'Купить акции компании cisco', 1),
(5, 'Уволить Крылова С.А.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pass` varchar(600) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `namepet` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `changepass` int(5) DEFAULT NULL,
  `grp` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `doclogin` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pet` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `pass`, `name`, `namepet`, `changepass`, `grp`, `doclogin`, `phone`, `pet`) VALUES
(4, 'vadia', 'vad.orov@gmail.com', '$2y$10$.XL55T6YgAy3tbj6pI/yse1UHYa/1GIoZmWfs5pkwSmOycRuQBgtK', 'Бялко Г.И.', 'Барсик', NULL, 'doctor', NULL, NULL, NULL),
(5, 'admin', 'vadd.orov@gmail.com', '$2y$10$o0vYNPRAa/NOf46o5oUmu.0/Hc7QoKU1aMCWspXoavyx5X..FMF56', 'Александр', '+', NULL, 'admin', NULL, '79885876696', NULL),
(6, 'user', 'vadia.orlov2015@yandex.ru', '$2y$10$o1ZJgpeTChV/dV.K/weJXO4sxIYbAxoprAUDQP9f92OsAzRINWiEC', 'Андрей', 'Котик', NULL, 'user', 'vadia', '+79938434234', NULL),
(10, 'nbhgkfded', 'vadd4у.oroцвv@gmail.com', '$2y$10$e9DSa509slO3EtZrK/cK2uzjAtIyUFh.ymUf109qCokA0AVaxACSK', 'Коля', 'Кот', NULL, 'user', NULL, '4321356535', 'Кот'),
(11, 'Tolya', 'rfdsefce@yandex.ru', '$2y$10$pVIuiLm4DSsiAK2AqJhqVe5PIF1WbthAcmyrYax0NT6JLMbO7iN3K', 'ТОЛИК', 'Пальма', NULL, 'user', NULL, '9048724324', 'Собака'),
(12, 'yanan1231', 'vaytred1.orytreweov@gmail.com', '$2y$10$1OuRFJDqlffe4LVfuWyKheVc4O5SMPrOrGQaQzQyhN5JI/gZ1fbwO', 'Ян', '', NULL, 'user', NULL, '234676623434', 'Собака'),
(13, 'KJdpf', 'vfsefsdia.osfsehrov4015@yandex.ru', '$2y$10$pBn.IgLKCcw8uqyF8MhnseF807BI7tU4e3zCOO49u2UjuSpnlrSZ2', 'Олег', 'Хома', NULL, 'user', NULL, '54323456765', 'Хомяк'),
(14, 'Ufurewx', 'vsaddtgd.orrroefv@gmail.com', '$2y$10$fyloAliuk6qLAd6hnpMxxO0OHUvXQJ3D6ZVx.SZ6V/CAhSBXPxz2K', 'Коля', '', NULL, 'user', NULL, '3456543234', 'Лягушка'),
(15, 'Olegsdfeg', 'tfcesvrv43@mail.ru', '$2y$10$JRz9IWxs75XWmDgMUHbb0OFt3ro3RuHK/KxfSWVxoOVmTNKalxMXq', 'Олег', '', NULL, 'user', NULL, '45678765432', 'Котик'),
(16, 'LJnovdcd', 'fva3rfd1.ofrfr4rfrfotv@gmail.com', '$2y$10$DibGF8KRBBt1Y5TgPJGkFOOvgdeGw2ha5kA2ZwKefs2rea3jKnn12', 'Дима', 'Компот', NULL, 'user', NULL, '12345673456543', 'Енот'),
(17, 'Koekmsfe', 'gtvagtdia.goglofrv20e315@yandex.ru', '$2y$10$GOEg8xJVJcHeDuI0jU3Cu.vKaRVu8eBb7A04.9rBXTbjWx74PBpj2', 'Алина', '', NULL, 'user', NULL, '23345344443', 'Зайчик'),
(18, 'moder', '111', '$2y$10$A8HQq2zK..JqLhaKpLB.zOVsDOdLF9pA9Ckzag4n5xtIxrgj5oFMm', 'Виктор', '1', NULL, 'moder', NULL, '111', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `zapis`
--

CREATE TABLE `zapis` (
  `id` int(11) NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `doctor` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `orderby` int(20) DEFAULT NULL,
  `doclogin` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zapis`
--

INSERT INTO `zapis` (`id`, `login`, `date`, `time`, `doctor`, `price`, `name`, `phone`, `status`, `orderby`, `doclogin`) VALUES
(22, NULL, '2021-07-07', '8:00', 'vadia', '500р', 'Саша', '+79938434234', 3, 800, NULL),
(23, NULL, '2021-07-07', '9:00', 'vadia', '700р', 'Андрей', '+798432', 3, 900, NULL),
(24, NULL, '2021-07-09', '8:00', 'Логин доктора', 'Цена', 'Саша', '+79938434234', 0, 800, NULL),
(25, 'user', '2021-07-10', '15:00', 'vadia', '700р', 'Андрей', '+79938434234', 1, 1500, 'vadia'),
(26, NULL, '2021-07-10', '9:00', 'vadia', '500р', 'Анна', '+798432', 3, 900, NULL),
(28, NULL, '2021-07-09', '20:00', 'Логин доктора', 'Цена', 'Андрей', '+798432', 0, 2000, NULL),
(29, NULL, '2021-07-11', '10:00', 'Логин доктора', 'Цена', 'Коля', '+42342342342', 0, 1000, NULL),
(30, NULL, '2021-07-12', '12:00', 'Логин доктора', 'Цена', 'Алексей', '+82525232321', 0, 1200, NULL),
(31, NULL, '2021-07-10', '18:00', 'Логин доктора', 'Цена', 'Олежа', '+99027377234', 0, 1800, NULL),
(32, NULL, '2021-07-10', '20:00', 'Логин доктора', 'Цена', 'Валера Петров', '+828372923273', 0, 2000, NULL),
(33, NULL, '2021-07-11', '8:00', 'Логин доктора', 'Цена', 'Маша', '+3847384343443', 0, 800, NULL),
(34, NULL, '2021-07-09', '11:00', 'Логин доктора', 'Цена', 'Ольга', '+827398382398', 0, 1100, NULL),
(38, 'admin', '2022-01-09', '11:00', 'Логин доктора', 'Цена', 'Владимир', '79885876696', 0, NULL, NULL),
(46, 'admin', '2022-01-10', '10:00', 'На уточнении', 'Цена', 'Владимир', '9885876696', 0, NULL, NULL),
(47, 'admin', '2022-01-10', '12:00', 'На уточнении', 'Цена', 'Саша', '131', 0, NULL, NULL),
(48, 'admin', '2022-01-10', '14:00', 'На уточнении', 'Цена', 'Миха', '1', 1, NULL, NULL),
(50, 'admin', '2022-01-13', '10:00', 'Бялко Г.И.', 'Цена', 'Аркадий', '+7973572643', 0, NULL, NULL),
(51, 'admin', '2022-01-14', '13:00', 'Бялко Г.И.', 'Цена', 'Сергей', '+73629499372', 0, NULL, 'Бялко Г.И.'),
(52, 'admin', '2022-01-14', '10:00', 'Бялко Г.И.', 'Цена', 'Евгений', '2345676543', 0, NULL, NULL),
(53, 'admin', '2022-01-15', '13:00', 'На уточнении', 'Цена', 'Сергей', '345676543', 3, NULL, NULL),
(54, 'admin', '2022-01-01', '10:00', 'На уточнении', 'Цена', 'Владимир', '79885876696', 0, NULL, NULL),
(55, 'admin', '2022-01-02', '11:00', 'На уточнении', 'Цена', 'Владимир', '+79885876696', 0, NULL, NULL),
(56, NULL, '2021-12-01', '8:00', 'Логин доктора', 'Цена', 'Саша', '+79938434234', 0, 800, NULL),
(57, 'user', '2021-12-01', '15:00', 'vadia', '700р', 'Андрей', '+79938434234', 1, 1500, 'vadia'),
(58, NULL, '2021-12-02', '9:00', 'vadia', '500р', 'Анна', '+798432', 3, 900, NULL),
(59, NULL, '2021-12-03', '20:00', 'Логин доктора', 'Цена', 'Андрей', '+798432', 0, 2000, NULL),
(60, NULL, '2021-12-03', '10:00', 'Логин доктора', 'Цена', 'Коля', '+42342342342', 0, 1000, NULL),
(61, NULL, '2021-12-03', '12:00', 'Логин доктора', 'Цена', 'Алексей', '+82525232321', 0, 1200, NULL),
(62, NULL, '2021-12-04', '18:00', 'Логин доктора', 'Цена', 'Олежа', '+99027377234', 0, 1800, NULL),
(63, NULL, '2021-12-04', '20:00', 'Логин доктора', 'Цена', 'Валера Петров', '+828372923273', 0, 2000, NULL),
(64, NULL, '2021-12-05', '8:00', 'Логин доктора', 'Цена', 'Маша', '+3847384343443', 0, 800, NULL),
(65, NULL, '2021-12-06', '11:00', 'Логин доктора', 'Цена', 'Ольга', '+827398382398', 0, 1100, NULL),
(66, 'admin', '2021-12-06', '11:00', 'Логин доктора', 'Цена', 'Владимир', '79885876696', 0, NULL, NULL),
(67, 'admin', '2021-12-07', '10:00', 'На уточнении', 'Цена', 'Владимир', '9885876696', 0, NULL, NULL),
(68, 'admin', '2021-12-08', '12:00', 'На уточнении', 'Цена', 'Саша', '131', 0, NULL, NULL),
(69, 'admin', '2021-12-08', '14:00', 'На уточнении', 'Цена', 'Миха', '1', 0, NULL, NULL),
(70, 'admin', '2021-12-09', '10:00', 'Бялко Г.И.', 'Цена', 'Аркадий', '+7973572643', 0, NULL, NULL),
(71, 'admin', '2021-12-10', '13:00', 'Бялко Г.И.', 'Цена', 'Сергей', '+73629499372', 0, NULL, 'Бялко Г.И.'),
(72, 'admin', '2021-12-11', '10:00', 'Бялко Г.И.', 'Цена', 'Евгений', '2345676543', 3, NULL, NULL),
(73, 'admin', '2021-12-12', '13:00', 'На уточнении', 'Цена', 'Сергей', '345676543', 0, NULL, NULL),
(74, 'admin', '2021-12-13', '10:00', 'На уточнении', 'Цена', 'Владимир', '79885876696', 1, NULL, NULL),
(75, 'admin', '2021-12-14', '11:00', 'На уточнении', 'Цена', 'Владимир', '+79885876696', 3, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authsess`
--
ALTER TABLE `authsess`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `DoctorRasp`
--
ALTER TABLE `DoctorRasp`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messagesfromcontact`
--
ALTER TABLE `messagesfromcontact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `todolist`
--
ALTER TABLE `todolist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `zapis`
--
ALTER TABLE `zapis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authsess`
--
ALTER TABLE `authsess`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `DoctorRasp`
--
ALTER TABLE `DoctorRasp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT для таблицы `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `messagesfromcontact`
--
ALTER TABLE `messagesfromcontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `todolist`
--
ALTER TABLE `todolist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `zapis`
--
ALTER TABLE `zapis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
